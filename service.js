const express = require("express");
const https = require("https");
const mysql = require("mysql");

const app = express();
app.use(express.json());

const conn = mysql.createConnection({
  host: "172.29.70.129",
  database: "trackingdrugs",
  user: "famz",
  password: "quiu7eedem",
});

conn.connect((err) => {
  if (err) throw err;

  app.post("/api/doctor/:service/", (req, res) => {
    let service = req.params.service;
    let result = [];
    let data;
    let body = req.body;
    let date_d;
    let code_b;
    let c_doct;
    let doctorPerid;

    switch (service) {
      case "isdoctor":
        doctorPerid = body.perid;
        data = JSON.stringify({
          service: "CHECKISDOCTOR",
          perid: doctorPerid,
        });
        break;
      case "clinic":
        doctorPerid = body.perid;
        date_d = body.date_d;
        data = JSON.stringify({
          service: "GETDOCTORDATA",
          perid: doctorPerid,
          date_d: date_d,
        });

        break;
      case "queue":
        date_d = body.date_d;
        code_b = body.code_b;
        c_doct = body.c_doct;
        data = JSON.stringify({
          service: "GETDOCTORPATIENTQUEUE",
          date_d: date_d,
          code_b: code_b,
          c_doct: c_doct,
        });
        break;
      default:
        break;
    }

    var options = {
      host: "his01.psu.ac.th",
      path: "/HosApp/clinicq/update2his.php",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Content-Length": Buffer.byteLength(data),
      },
      rejectUnauthorized: false,
    };

    // Call service in update2his.php
    var request = https.request(options, (httpres) => {
      var resdata = "";
      httpres.on("data", function (chunk) {
        resdata += chunk;
      });
      httpres.on("end", function () {
        let dataJSON = JSON.parse(resdata);
        let dataresponse = [];
        switch (service) {
          case "isdoctor":
            if (dataJSON.length != 0) {
              dataJSON.forEach((element) => {
                dataresponse.push({
                  doctorname: element[0] + " " + element[1],
                  sex: element[2],
                });
              });
              result = {
                statusCode: 200,
                date: new Date(),
                service: req.params.service,
                data: dataresponse,
              };
            } else {
              result = {
                statusCode: 400,
                date: new Date(),
                service: req.params.service,
                data: [],
              };
            }
            break;
          case "clinic":
            if (dataJSON.length != 0) {
              dataJSON.forEach((element) => {
                doctorname = element[1] + " " + element[2];
                dataresponse.push({
                  c_doct: element[0],
                  date_d: element[3],
                  clinic_code: element[4],
                  clinic_name: element[5],
                });
              });
              result = {
                statusCode: 200,
                date: new Date(),
                service: req.params.service,
                data: dataresponse,
              };
            } else {
              result = {
                statusCode: 400,
                date: new Date(),
                service: req.params.service,
                data: [],
              };
            }

            break;
          case "queue":
            if (dataJSON.length != 0) {
              dataJSON.forEach((element) => {
                dataresponse.push({
                  hn: element[0],
                  patientname: element[10] + " " + element[11],
                  sex: element[12],
                  date_d: element[1],
                  regist_time: element[3],
                  q_time: element[4],
                  q_doct_time: element[5],
                  doct_time: element[6],
                  date_close: element[7],
                  q_exit_nurse: element[8],
                  nurse_time: element[9],
                });
              });
              result = {
                statusCode: 200,
                date: new Date(),
                service: req.params.service,
                data: dataresponse,
              };
            } else {
              result = {
                statusCode: 400,
                date: new Date(),
                service: req.params.service,
                data: [],
              };
            }
            break;
          default:
            break;
        }
        res.send(result);
      });
    });
    request.on("error", function (e) {
      let error = {
        statusCode: 400,
        error: e.message,
      };
      console.log(error);
    });
    request.write(data);
    request.end();
  });

  app.post("/api/patient/:service/", (req, res) => {
    let service = req.params.service;
    let body = req.body;
    let data;
    let patienthn;
    let code_b;
    let date_d;
    let doctorPerid;
    let hn;
    switch (service) {
      case "info":
        patienthn = body.hn;
        data = JSON.stringify({
          service: "GETPATIENTINFO",
          hn: patienthn,
        });
        break;
      case "time":
        date_d = body.date_d;
        code_b = body.code_b;
        doctorPerid = body.c_doct;
        hn = body.hn;

        data = JSON.stringify({
          service: "GETPATIENTTIME",
          date_d: date_d,
          code_b: code_b,
          c_doct: doctorPerid,
          hn: hn,
        });
        break;
      case "timeinsert":
        date_d = body.date_d;
        code_b = body.code_b;
        doctorPerid = body.c_doct;
        hn = body.hn;
        data = JSON.stringify({
          service: "INSERTPATIENTTIME",
          date_d: date_d,
          code_b: code_b,
          c_doct: doctorPerid,
          hn: hn,
        });
        break;
      case "timeupdate":
        date_d = body.date_d;
        code_b = body.code_b;
        doctorPerid = body.c_doct;
        hn = body.hn;
        data = JSON.stringify({
          service: "UPDATEPATIENTTIME",
          date_d: date_d,
          code_b: code_b,
          c_doct: doctorPerid,
          hn: hn,
        });
        break;
      case "clear":
        hn = body.hn;
        code_b = body.code_b;
        doctorPerid = body.c_doct;
        data = JSON.stringify({
          service: "CLEARPATIENTTIME",
          hn: hn,
          code_b: code_b,
          c_doct: doctorPerid,
        });
        break;
      default:
        break;
    }

    var options = {
      host: "his01.psu.ac.th",
      path: "/HosApp/clinicq/update2his.php",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Content-Length": Buffer.byteLength(data),
      },
      rejectUnauthorized: false,
    };

    // Call service in update2his.php
    var request = https.request(options, (httpres) => {
      var resdata = "";
      httpres.on("data", function (chunk) {
        resdata += chunk;
      });
      httpres.on("end", function () {
        let dataJSON = JSON.parse(resdata);
        let dataresponse = [];
        switch (service) {
          case "info":
            if (dataJSON.length != 0) {
              dataJSON.forEach((element) => {
                dataresponse.push({
                  hn: element[0],
                  fullname: element[1] + " " + element[2],
                });
              });
              result = {
                statusCode: 200,
                date: new Date(),
                service: req.params.service,
                data: dataresponse,
              };
            } else {
              result = {
                statusCode: 400,
                date: new Date(),
                service: req.params.service,
                data: [],
              };
            }
            break;
          case "time":
            // หาก dataJSON ว่าง ทำการ insert ใหม่
            // หาก dataJSON ไม่ว่าง ทำการ update ข้อมูลเดิม

            if (dataJSON.length != 0) {
              dataJSON.forEach((element) => {
                dataresponse.push({
                  hn: element[0],
                  date_d: element[1],
                  code_b:element[2],
                  c_doct:element[3]
                });
              });
              result = {
                statusCode: 200,
                date: new Date(),
                data: dataresponse,
              };
            } else {
              result = {
                statusCode: 200,
                date: new Date(),
                data: [],
              };
            }
            break;
          case "timeupdate":
            result = {
              statusCode: 200,
              date: new Date(),
              data: "Update success",
            };
            break;
          case "timeinsert":
            result = {
              statusCode: 200,
              date: new Date(),
              data: "Insert success",
            };
            break;
          case "clear":
            result = {
              statusCode: 200,
              date: new Date(),
              data: "Clear success",
            };
            break;
          default:
            break;
        }
        res.send(result);
      });
    });
    request.on("error", function (e) {
      let error = {
        statusCode: 400,
        error: e.message,
      };
      console.log(error);
    });
    request.write(data);
    request.end();
  });
});

const port = process.env.PORT || 5336;
app.listen(port, "0.0.0.0", () =>
  console.log("Listening on port " + port + "...")
);
